<?php
class API
{
    protected $url;
    function __construct($path)
    {
        $this->url = 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/' . $path;
    }
    public function getData()
    {
        $cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $this->url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        return json_decode($data);
    }
}

class Travel extends API
{
    // Enter your code here
    protected $data;
    function __construct()
    {
        parent::__construct('travels');
        $this->data = $this->getData();
    }
    public function getCostByCompanyIds($company_ids)
    {
        return array_reduce($this->data, function ($cost, $item) use ($company_ids) {
            if (in_array($item->companyId, $company_ids)) {
                $cost += $item->price;
            }
            return $cost;
        });
    }
}

class Company extends API
{
    // Enter your code here
    public $data;
    protected $travel;
    function __construct(Travel $travel)
    {   
        parent::__construct('companies');
        $this->travel = $travel;
        $this->data = $this->getData();
    }
    public function renderTree(array &$elements, $parentId = '0')
    {
        $branch = array();

        foreach ($elements as $element) {
            $element = (array) $element;
            if ($element['parentId'] == $parentId) {
                $children = $this->renderTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $childIds = self::getchildren($this->data, $element['id']);
                $childIds[] = $element['id'];
                $element['cost'] = $this->travel->getCostByCompanyIds($childIds);
                $branch[] = $element;
            }
        }

        return $branch;
    }

    private static function getchildren($src_arr, $parent_id = 'uuid-1')
    {
        $allchildren = array();

        foreach ($src_arr as $row) {
            if ($row->parentId == $parent_id) {
                $allchildren[] = $row->id;
                $allchildren = array_merge($allchildren, self::getchildren($src_arr, $row->id));
            }
        }
        return $allchildren;
    }
}

class TestScript
{
    public function execute()
    {
        $start = microtime(true);
        $travel = new Travel;
        $company = new Company($travel);
        $company_data = $company->renderTree($company->data);
        var_dump($company_data);
        echo 'Total time: ' .  (microtime(true) - $start);
    }
}

(new TestScript())->execute();
